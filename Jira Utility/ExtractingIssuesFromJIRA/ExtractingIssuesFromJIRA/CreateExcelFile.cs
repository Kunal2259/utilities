﻿namespace ExtractingIssuesFromJIRA
{
    using Microsoft.Office.Interop.Excel;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;

    public class CreateExcelFile
    {
        private String projectName = string.Empty;
        private string filePath = string.Empty;
        static private Microsoft.Office.Interop.Excel.Application xlApp;
        static private Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        static private Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
        private object misValue;
        static int i = 2;
        static System.Data.DataTable dataTable = null;

        public CreateExcelFile(string projectName, string filePath)
        {
            this.projectName = projectName;
            this.filePath = filePath;
            dataTable = new System.Data.DataTable();
        }

        public string CreatingExcelHeader()
        {
            string retVal = "OK";
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                retVal = "Excel is not properly installed!!";
            }
            else
            {
                misValue = System.Reflection.Missing.Value;
                //xlWorkBook = xlApp.Workbooks.Add(misValue);
                //xlWorkBook = xlApp.Workbooks.Open("C:\\DSR\\DHS_OIT_DailyDashboard.xlsx",
                //                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                //                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                //                    Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                string[] allFiles = Directory.GetFiles("C:\\DSR\\Input");
                xlWorkBook = xlApp.Workbooks.Open(allFiles[0],
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(4);
                
                if (xlWorkSheet.AutoFilter != null)
                {
                    xlWorkSheet.AutoFilterMode = false;
                }

                Microsoft.Office.Interop.Excel.Range formatRange;
                xlWorkSheet.get_Range("a1", "r" + xlWorkSheet.UsedRange.Rows.Count).ClearFormats();
                xlWorkSheet.get_Range("a1", "r" + xlWorkSheet.UsedRange.Rows.Count).ClearContents();
                
                //formatRange = xlWorkSheet.get_Range("a1", "p1");
                //formatRange.ClearContents();
                //formatRange = xlWorkSheet.get_Range("a1");
                //xlWorkSheet.Cells[1, 1] = "Project";
                //xlWorkSheet.Cells[1, 2] = "Key (JIRA ID)";
                //xlWorkSheet.Cells[1, 3] = "Issue Type";
                //xlWorkSheet.Cells[1, 4] = "Summary";
                //xlWorkSheet.Cells[1, 5] = "Status";
                //xlWorkSheet.Cells[1, 6] = "Priority";
                //xlWorkSheet.Cells[1, 7] = "Points";
                //xlWorkSheet.Cells[1, 8] = "Created Date";
                //xlWorkSheet.Cells[1, 9] = "Business Unit";
                //xlWorkSheet.Cells[1, 10] ="Business Requirement";
                //xlWorkSheet.Cells[1, 11] ="Reporter";
                //xlWorkSheet.Cells[1, 12] ="Sprint #";
                //xlWorkSheet.Cells[1, 13] ="Defect Cause";
                //xlWorkSheet.Cells[1, 14] ="Effort(in Hrs)";
                //xlWorkSheet.Cells[1, 15] ="Success Criteria Tag";
                //xlWorkSheet.Cells[1, 16] ="Updated Date";
                //xlWorkSheet.Cells[1, 17] ="Created Date";
                //dataTable.Rows.Add("Project", "Key (JIRA ID)", "Issue Type", "Summary", "Status", "Priority", "Points", "Created Date", "Business Unit", "Business Requirement", "Reporter",
                //                    "Sprint #", "Defect Cause", "Effort(in Hrs)", "Success Criteria Tag", "Updated Date");
                dataTable.Columns.Add("Project");
                dataTable.Columns.Add("Key (JIRA ID)");
                dataTable.Columns.Add("Issue Type");
                dataTable.Columns.Add("Summary");
                dataTable.Columns.Add("Status");
                dataTable.Columns.Add("Priority");
                dataTable.Columns.Add("Points");
                dataTable.Columns.Add("Created Date");
                dataTable.Columns.Add("Business Unit");
                dataTable.Columns.Add("Business Requirement");
                dataTable.Columns.Add("Reporter");
                dataTable.Columns.Add("Sprint #");
                dataTable.Columns.Add("Defect Cause");
                dataTable.Columns.Add("Effort(in Hrs)");
                dataTable.Columns.Add("Success Criteria Tag");
                dataTable.Columns.Add("Updated Date");
                dataTable.Columns.Add("Resolution");
                dataTable.Columns.Add("Epic Link");
                dataTable.Rows.Add("Project", "Key (JIRA ID)", "Issue Type", "Summary", "Status", "Priority", "Points", "Created Date", "Business Unit", "Business Requirement", "Reporter",
                                    "Sprint #", "Defect Cause", "Effort(in Hrs)", "Success Criteria Tag", "Updated Date",
                                    "Resolution", "Epic Link");
                formatRange = xlWorkSheet.get_Range("a1", "r1");
                formatRange.Interior.Color = System.Drawing.Color.Cyan;
                formatRange.Font.Bold = true;
                formatRange.Font.Name = "Times New Roman";
                formatRange.WrapText = true;
                formatRange.Font.Size = 18;
                formatRange.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                //dataTable.Rows.Add("1", "lhbi", "", "", 2, 4, 2, "12/10/2016", "", "", "", "", "", 3, "12/10/2016");
                //dataTable.Rows.Add("2", "", "", "loib", 2, 4, 2, "12/10/2016", "", "", "", "sa", "", 3, "12/10/2016");
                //dataTable.Rows.Add("3", "", "", "", 2, 4, 2, "12/10/2016", "", "", "", "", "", 3, "12/10/2016");
                //dataTable.Rows.Add("4", "", "", "", 2, 4, 2, "12/10/2016", "", "lkibh", "", "", "likbi", 3, "12/10/2016");
                //formatRange = xlWorkSheet.get_Range("a2", "p" + dataTable.Rows.Count);
                object[,] arr = new object[dataTable.Rows.Count, dataTable.Columns.Count];
                for (int r = 0; r < dataTable.Rows.Count; r++)
                {
                    System.Data.DataRow dr = dataTable.Rows[r];
                    for (int c = 0; c < dataTable.Columns.Count; c++)
                    {
                        arr[r, c] = dr[c];
                    }
                }
                formatRange.Value = arr;
                dataTable.Rows.RemoveAt(0);
            }
            return retVal;
        }

        public string PopulatingDataInExcel(dynamic dataFromJiraServer)
        {
            string retVal = "OK";

            try
            {
                string sprint;//= rr.issues[5].fields.customfield_10007.ToString();
                int nameIndex;// = sprint.IndexOf("name=" + 5);
                string date;
                Console.WriteLine("Total issues fetched : " + dataFromJiraServer.issues.Count);
                string val1, val2, val3, val4, val5, val6, val7, val8, val9, val10, val11, val12, val13, val14, val15, val16, val17, val18;
                DateTime dateTime;
                TimeZoneInfo tzLocal = TimeZoneInfo.Local;
                TimeZoneInfo tzEST = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                Dictionary<string, string> dict = new Dictionary<string, string>();
                for (int localCount = 0; localCount < dataFromJiraServer.issues.Count; localCount++)
                {
                    val3 = dataFromJiraServer.issues[localCount].fields.issuetype != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name.ToString() : String.Empty : String.Empty;
                    if (String.Equals(val3, "Epic", StringComparison.OrdinalIgnoreCase))
                    {
                        val2 = dataFromJiraServer.issues[localCount].key != null ? dataFromJiraServer.issues[localCount].key.ToString() : String.Empty;
                        val4 = dataFromJiraServer.issues[localCount].fields.summary != null ? dataFromJiraServer.issues[localCount].fields.summary.ToString() : String.Empty;
                        dict.Add(val2, val4);
                    }
                }
                TimeZone zone = TimeZone.CurrentTimeZone;
                string standard = zone.StandardName;
                for (int localCount = 0; localCount < dataFromJiraServer.issues.Count; localCount++)
                {
                    sprint = dataFromJiraServer.issues[localCount].fields.customfield_10007 != null ? dataFromJiraServer.issues[localCount].fields.customfield_10007.ToString() : String.Empty;
                    nameIndex = sprint.IndexOf("name=") == -1 ? sprint.IndexOf("name=") : sprint.IndexOf("name=") + 5;
                    if (nameIndex != -1)
                    {
                        sprint = sprint.Substring(nameIndex, sprint.IndexOf(",", nameIndex) - nameIndex);
                    }

                    val1 = dataFromJiraServer.issues[localCount].fields.project != null ? dataFromJiraServer.issues[localCount].fields.project.name != null ? dataFromJiraServer.issues[localCount].fields.project.name.ToString() : String.Empty : String.Empty;
                    val2 = dataFromJiraServer.issues[localCount].key != null ? dataFromJiraServer.issues[localCount].key.ToString() : String.Empty;
                    val3 = dataFromJiraServer.issues[localCount].fields.issuetype != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name.ToString() : String.Empty : String.Empty;
                    val4 = dataFromJiraServer.issues[localCount].fields.summary != null ? dataFromJiraServer.issues[localCount].fields.summary.ToString() : String.Empty;
                    val5 = dataFromJiraServer.issues[localCount].fields.status != null ? dataFromJiraServer.issues[localCount].fields.status.name != null ? dataFromJiraServer.issues[localCount].fields.status.name.ToString() : String.Empty : String.Empty;
                    val6 = dataFromJiraServer.issues[localCount].fields.priority != null ? dataFromJiraServer.issues[localCount].fields.priority.name != null ? dataFromJiraServer.issues[localCount].fields.priority.name.ToString() : String.Empty : String.Empty;
                    val7 = dataFromJiraServer.issues[localCount].fields.customfield_10005 != null ? dataFromJiraServer.issues[localCount].fields.customfield_10005.ToString() : String.Empty;
                    //date = dataFromJiraServer.issues[localCount].fields.created != null ? dataFromJiraServer.issues[localCount].fields.created.ToString() : String.Empty;
                    ////date = date.Substring(0, date.IndexOf(" "));
                    ////xlWorkSheet.Cells[i, 8] = date;
                    //date = dataFromJiraServer.issues[localCount].fields.created != null ? dataFromJiraServer.issues[localCount].fields.created.ToString() : String.Empty;
                    dateTime = dataFromJiraServer.issues[localCount].fields.created != null ? dataFromJiraServer.issues[localCount].fields.created : null;
                    //val8 = date.Substring(0, date.IndexOf(" "));
                    //dateTime = dateTime.AddHours(14.30);
                    if (standard != "Eastern Standard Time")
                    {
                        dateTime = TimeZoneInfo.ConvertTime(dateTime, tzLocal, tzEST);
                    }
                    val8 = dateTime == null ? "" : String.Format("{0:MM/dd/yyyy}", dateTime);
                    //date = dataFromJiraServer.issues[localCount].fields.created != null ? dataFromJiraServer.issues[localCount].fields.created.ToString() : String.Empty;
                    //xlWorkSheet.Cells[i, 17] = date.Substring(0, date.IndexOf(" "));
                    val9  = dataFromJiraServer.issues[localCount].fields.customfield_10600 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10600[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10600[0].value.ToString() : String.Empty) : String.Empty;
                    val11 = dataFromJiraServer.issues[localCount].fields.reporter != null ? dataFromJiraServer.issues[localCount].fields.reporter.displayName != null ? dataFromJiraServer.issues[localCount].fields.reporter.displayName.ToString() : String.Empty : String.Empty;
                    val10 = dataFromJiraServer.issues[localCount].fields.customfield_10027 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10027[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10027[0].value.ToString() : String.Empty) : String.Empty;
                    val12 = sprint;
                    val13 = dataFromJiraServer.issues[localCount].fields.customfield_10400 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10400[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10400[0].value.ToString() : String.Empty) : String.Empty;
                    val14 = dataFromJiraServer.issues[localCount].fields.timespent != null ? (Convert.ToInt64(dataFromJiraServer.issues[localCount].fields.timespent.ToString()) / 3600).ToString() : string.Empty;
                    //xlWorkSheet.Cells[i, 15] =
                    //date = dataFromJiraServer.issues[localCount].fields.updated != null ? dataFromJiraServer.issues[localCount].fields.updated.ToString() : String.Empty;
                    dateTime = dataFromJiraServer.issues[localCount].fields.updated != null ? dataFromJiraServer.issues[localCount].fields.updated : null;
                    //dateTime = dateTime.AddHours(-14.30);
                    dateTime = TimeZoneInfo.ConvertTime(dateTime, tzLocal, tzEST);
                    val15 = "";
                    //val16 = date.Substring(0, date.IndexOf(" "));
                    val16 = dateTime == null ? "" : String.Format("{0:MM/dd/yyyy}", dateTime);
                    //val17 = string.Empty;
                    val17 = dataFromJiraServer.issues[localCount].fields.resolution != null ? dataFromJiraServer.issues[localCount].fields.resolution.name != null ? dataFromJiraServer.issues[localCount].fields.resolution.name.ToString() : String.Empty : String.Empty;
                    val18 = dataFromJiraServer.issues[localCount].fields.customfield_10009 != null ? dataFromJiraServer.issues[localCount].fields.customfield_10009.ToString() : String.Empty;
                    if (dict.ContainsKey(val18))
                    {
                        val18 = dict[val18];
                    }
                    dataTable.Rows.Add(val1, val2, val3, val4, val5, val6, val7, val8, val9, val10, val11, val12, val13, val14, val15, val16, val17, val18);
                    i++;//customfield_10008
                }

                //Range rowrange = xlWorkSheet.get_Range

                //Range dataRange = xlWorkSheet.get_Range("a1", "p1");

                //dataRange.
                Microsoft.Office.Interop.Excel.Range formatRange = xlWorkSheet.get_Range("a2", "r" + (dataTable.Rows.Count+1));
                formatRange.Font.Name = "Times New Roman";
                formatRange.Font.Size = 15;
                formatRange.WrapText = true;
                formatRange.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                object[,] arr = new object[dataTable.Rows.Count, dataTable.Columns.Count];
                for (int r = 0; r < dataTable.Rows.Count; r++)
                {
                    System.Data.DataRow dr = dataTable.Rows[r];
                    for (int c = 0; c < dataTable.Columns.Count; c++)
                    {
                        arr[r, c] = dr[c];
                    }
                }
                formatRange.Value = arr; 
            }
            catch (Exception ex)
            {
                retVal = "Something happened wrong" + ex.Message + "  " + i + "  populating";
            }
            return retVal;
        }

        public string SavingExcel()
        {
            string retVal = "OK";
            try
            {
                string fileName = "DHS_OIT_DailyDashboard" + " " + projectName + " " + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Year + " " + DateTime.Now.TimeOfDay.Hours + "-"
                                  + DateTime.Now.TimeOfDay.Minutes + "-" + DateTime.Now.TimeOfDay.Seconds;
                //string path = "E:\\CreatingReports";
                if (!System.IO.Directory.Exists(filePath))
                {
                    System.IO.Directory.CreateDirectory(filePath);
                }
                xlWorkSheet.Columns.AutoFit();
                //Microsoft.Office.Interop.Excel.Application xlApp1 = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook1 = xlWorkBook;
                //Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                //xlWorkBook1.SaveAs(filePath + "\\" + fileName + ".xlsx", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.SaveCopyAs(filePath + "\\" + fileName + ".xlsx");
                //Marshal.ReleaseComObject(xlWorkBook);
                xlWorkBook.Close(false, misValue, misValue);
                Marshal.ReleaseComObject(xlWorkBook);
                xlApp.Quit();
                Process[] processes = Process.GetProcessesByName("EXCEL");
                processes[processes.Length-1].Kill();
            }
            catch (Exception ex)
            {
                retVal = "Something happened wrong" + ex.Message + " saving";
            }
            return retVal;
        }
    }
}
