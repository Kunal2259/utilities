﻿namespace ExtractingIssuesFromJIRA
{
    using System;

    public static class CreatingExcel
    {
        static Microsoft.Office.Interop.Excel.Application xlApp;
        static Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        static Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
        static object misValue;
        static String projectName;
        static string filePath = string.Empty;
        static int i = 2;

        public static string CreateExcel(string pName, dynamic dataFromJiraServer, string path)
        {
            projectName = pName;
            filePath = path;
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                return "Excel is not properly installed!!";
            }
            else
            {
                misValue = System.Reflection.Missing.Value;
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                CreatingExcelHeader();
                string response = PopulatingDataInExcel(dataFromJiraServer);
                response = response == "OK" ? SavingExcel() : response;
                return response;
            }
        }

        public static string CreateExcelForAll(string pName, dynamic dataFromJiraServer, bool headerCreated , bool saveExcel, string path)
        {
            projectName = pName;
            filePath = path;
            xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                return "Excel is not properly installed!!";
            }
            else
            {
                if (headerCreated == true)
                {
                    misValue = System.Reflection.Missing.Value;
                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    CreatingExcelHeader();
                }
                string response = PopulatingDataInExcel(dataFromJiraServer);

                if (response == "OK" && saveExcel == true)
                {
                    response = SavingExcel();
                }
                return response;
            }
        }

        private static void CreatingExcelHeader()
        {
            Microsoft.Office.Interop.Excel.Range formatRange;
            formatRange = xlWorkSheet.get_Range("a1");
            xlWorkSheet.Cells[1, 1] = "Project";
            xlWorkSheet.Cells[1, 2] = "Key (JIRA ID)";
            xlWorkSheet.Cells[1, 3] = "Issue Type";
            xlWorkSheet.Cells[1, 4] = "Summary";
            xlWorkSheet.Cells[1, 5] = "Status";
            xlWorkSheet.Cells[1, 6] = "Priority";
            xlWorkSheet.Cells[1, 7] = "Points";
            xlWorkSheet.Cells[1, 8] = "Created Date";
            xlWorkSheet.Cells[1, 9] = "Business Unit";
            xlWorkSheet.Cells[1, 10] = "Business Requirement";
            xlWorkSheet.Cells[1, 11] = "Reporter";
            xlWorkSheet.Cells[1, 12] = "Sprint #";
            xlWorkSheet.Cells[1, 13] = "Defect Cause";
            xlWorkSheet.Cells[1, 14] = "Effort(in Hrs)";
            xlWorkSheet.Cells[1, 15] = "Success Criteria Tag";
            formatRange = xlWorkSheet.get_Range("a1", "o1");
            formatRange.Interior.Color = System.Drawing.Color.Cyan;
        }

        private static string PopulatingDataInExcel(dynamic dataFromJiraServer)
        {
            string retVal = "OK";
            try
            {
                string sprint;//= rr.issues[5].fields.customfield_10007.ToString();
                int nameIndex;// = sprint.IndexOf("name=" + 5);
                for (int localCount = 0 ; localCount < dataFromJiraServer.issues.Count; localCount++)
                {
                    sprint = dataFromJiraServer.issues[localCount].fields.customfield_10007 != null ? dataFromJiraServer.issues[localCount].fields.customfield_10007.ToString() : String.Empty;
                    nameIndex = sprint.IndexOf("name=") == -1 ? sprint.IndexOf("name=") : sprint.IndexOf("name=") + 5;
                    if (nameIndex != -1)
                    {
                        sprint = sprint.Substring(nameIndex, sprint.IndexOf(",", nameIndex) - nameIndex);
                    }

                    xlWorkSheet.Cells[i, 1] = dataFromJiraServer.issues[localCount].fields.project != null ? dataFromJiraServer.issues[localCount].fields.project.name != null ? dataFromJiraServer.issues[localCount].fields.project.name.ToString() : String.Empty : String.Empty;
                    xlWorkSheet.Cells[i, 2] = dataFromJiraServer.issues[localCount].key != null ? dataFromJiraServer.issues[localCount].key.ToString() : String.Empty;
                    xlWorkSheet.Cells[i, 3] = dataFromJiraServer.issues[localCount].fields.issuetype != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name != null ? dataFromJiraServer.issues[localCount].fields.issuetype.name.ToString() : String.Empty : String.Empty;
                    xlWorkSheet.Cells[i, 4] = dataFromJiraServer.issues[localCount].fields.summary != null ? dataFromJiraServer.issues[localCount].fields.summary.ToString() : String.Empty;
                    xlWorkSheet.Cells[i, 5] = dataFromJiraServer.issues[localCount].fields.status != null ? dataFromJiraServer.issues[localCount].fields.status.name != null ? dataFromJiraServer.issues[localCount].fields.status.name.ToString() : String.Empty : String.Empty;
                    xlWorkSheet.Cells[i, 6] = dataFromJiraServer.issues[localCount].fields.priority != null ? dataFromJiraServer.issues[localCount].fields.priority.name != null ? dataFromJiraServer.issues[localCount].fields.priority.name.ToString() : String.Empty : String.Empty;
                    xlWorkSheet.Cells[i, 7] = dataFromJiraServer.issues[localCount].fields.customfield_10005 != null ? dataFromJiraServer.issues[localCount].fields.customfield_10005.ToString() : String.Empty;
                    xlWorkSheet.Cells[i, 8] = dataFromJiraServer.issues[localCount].fields.created != null ? dataFromJiraServer.issues[localCount].fields.created.ToString() : String.Empty;
                    xlWorkSheet.Cells[i, 9] = dataFromJiraServer.issues[localCount].fields.customfield_10600 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10600[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10600[0].value.ToString() : String.Empty) : String.Empty;
                    xlWorkSheet.Cells[i, 11] = dataFromJiraServer.issues[localCount].fields.reporter != null ? dataFromJiraServer.issues[localCount].fields.reporter.displayName != null ? dataFromJiraServer.issues[localCount].fields.reporter.displayName.ToString() : String.Empty : String.Empty;
                    xlWorkSheet.Cells[i, 10] = dataFromJiraServer.issues[localCount].fields.customfield_10027 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10027[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10027[0].value.ToString() : String.Empty) : String.Empty;
                    xlWorkSheet.Cells[i, 12] = sprint;
                    xlWorkSheet.Cells[i, 13] = dataFromJiraServer.issues[localCount].fields.customfield_10400 != null ? (dataFromJiraServer.issues[localCount].fields.customfield_10400[0].value != null ? dataFromJiraServer.issues[localCount].fields.customfield_10400[0].value.ToString() : String.Empty) : String.Empty;
                    xlWorkSheet.Cells[i, 14] = dataFromJiraServer.issues[localCount].fields.timespent != null ? (Convert.ToInt64(dataFromJiraServer.issues[localCount].fields.timespent.ToString()) / 3600).ToString() : string.Empty;
                    //xlWorkSheet.Cells[i, 15] =
                    i++;
                }
            }
            catch (Exception ex)
            {
                retVal = "Something happened wrong"+ ex.Message + "  " + i + "  populating";
            }
            return retVal;
        }

        private static string SavingExcel()
        {
            string retVal = "OK";
            try
            {
                string fileName = projectName + " " + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Year + " " + DateTime.Now.TimeOfDay.Hours + "-"
                                  + DateTime.Now.TimeOfDay.Minutes + "-" + DateTime.Now.TimeOfDay.Seconds;
                ////string path = "E:\\CreatingReports";
                //if (!System.IO.Directory.Exists(filePath))
                //{
                //    System.IO.Directory.CreateDirectory(filePath);
                //}
                xlWorkBook.SaveAs(filePath + "\\" + fileName + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                retVal = "Something happened wrong" + ex.Message + " saving";
            }
            return retVal;
        }
    }
}
