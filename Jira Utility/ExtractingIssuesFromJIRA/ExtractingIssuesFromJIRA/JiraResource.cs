﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractingIssuesFromJIRA
{
    public enum JiraResource
    {
        //HRIS = "HRIS", 
        //BCS = "BCSB",                                                    
        //VETS = "VTS",                                                  
        //ORCA = "LO",                                              
        ////SharePoint,
        //HomeStat = "HOMESTAT",                                                       
        //ServiceNow = "SNIHD"     
        HRIS,
        BCSB,
        VTS,
        LO,
        HOMESTAT,
        SNIHD,
        CARES,
        SCDN,
        NTS,
        NSP,
        CD,
        HOME,
        IM,
        IW,
        QCHWA,
        ALL
    }
    
}
