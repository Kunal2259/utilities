﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractingIssuesFromJIRA.ExtensionMethod
{
    static class DataTableExtensionMethod
    {
        public static string[,] ToStringArray(this System.Data.DataTable table)
        {
            DateTime datetime;
            string[,] importString = new string[table.Rows.Count + 1, table.Columns.Count];
            for (int columnCounter = 0; columnCounter < table.Columns.Count; columnCounter++)
            {
                importString[0, columnCounter] = table.Columns[columnCounter].ColumnName;
            }
            for (int rowCounter = 0; rowCounter < table.Rows.Count; rowCounter++)
            {
                for (int columnCounter = 0; columnCounter < table.Columns.Count; columnCounter++)
                {
                    if (DateTime.TryParse(table.Rows[rowCounter][columnCounter].ToString(), out datetime))
                    {
                        string date = Convert.ToString(datetime.Date.ToString("MM/dd/yy"));
                        importString[rowCounter + 1, columnCounter] = date;
                    }
                    else
                    {
                        importString[rowCounter + 1, columnCounter] = table.Rows[rowCounter][columnCounter].ToString();
                    }
                }
            }
            return importString;
        }

    }
}
