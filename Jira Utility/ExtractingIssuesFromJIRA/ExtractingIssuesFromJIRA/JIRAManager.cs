﻿namespace ExtractingIssuesFromJIRA
{
    using Newtonsoft.Json.Linq;
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    public class JIRAManager
    {
        private const string m_BaseUrl = "https://nycdhs.atlassian.net/rest/api/2/search?jql=project=";
        private string m_Username;
        private string m_Password;

        public JIRAManager(string username, string password)
        {
            m_Username = username;
            m_Password = password;
        }

        /// <summary>
        /// Running the query to fetch JSON data
        /// </summary>
        /// <param name="resource">Project</param>
        public dynamic RunQuery(
            JiraResource resource,
            string argument = null,
            string data = null,
            string method = "GET",
            int startAt = 1)
        {
            try
            {//https://nycdhs.atlassian.net/rest/api/2/search?jql=project=SNIHD+&maxResults=1000&startAt=1
                ///https://nycdhs.atlassian.net/rest/api/2/tempo-timesheets/3/timesheet-approval/
                string url = string.Format("{0}{1}", m_BaseUrl, resource.ToString());
                url = url + "&maxResults=1000&startAt="+startAt;
                if (argument != null)
                {
                    url = string.Format("{0}{1}/", url, argument);
                }

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.ContentType = "application/json";
                request.Method = method;

                if (data != null)
                {
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                    {
                        writer.Write(data);
                    }
                }

                string base64Credentials = GetEncodedCredentials();
                request.Headers.Add("Authorization", "Basic " + base64Credentials);

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                string result = string.Empty;
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }
                JObject parsedJsonData = JObject.Parse(result);
                return parsedJsonData;
            }
            catch (Exception ex)
            {
                return "Exception";
            }
        }

        /// <summary>
        /// For encoding credentials
        /// </summary>
        /// <returns></returns>
        private string GetEncodedCredentials()
        {
            string mergedCredentials = string.Format("{0}:{1}", m_Username, m_Password);
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }

        //private createIssue()
        //{

        //}
    }
}
