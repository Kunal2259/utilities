﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExtractingIssuesFromJIRA
{
    using System.Configuration;
    using System.IO;
    class Program
    {
        static void Main(string[] args)
        {
            if (!System.IO.Directory.Exists(@"c:\DSR\Input"))
            {
                System.IO.Directory.CreateDirectory(@"c:\DSR\Input");
                if (!System.IO.Directory.Exists(@"c:\DSR\Output"))
                {
                    System.IO.Directory.CreateDirectory(@"c:\DSR\Output");
                }
                Console.WriteLine(@"Please place your input file in C:\DSR\Input directory");
                Console.ReadLine();
            }
            else
            {
                try
                {
                    int allFilesCount = new DirectoryInfo("C:\\DSR\\Input").GetFiles()
                                                            .Count(f => !f.Attributes.HasFlag(FileAttributes.Hidden));
                    if (allFilesCount != 1)
                    {
                        Console.WriteLine("Error : you can keep only 1 file in C:\\DSR\\Input directory");
                        Console.ReadLine();
                    }
                    else
                    {
                        JObject dataFromJiraServer = null;// = manager.RunQuery(JiraResource.BCSB);
                        string filePath = string.Empty;

                        string response = string.Empty, printValue = string.Empty;//= CreatingExcel.CreateExcel(JiraResource.BCSB.ToString(), dataFromJiraServer);
                        if (CheckForInternetConnection())
                        {
                            ShowStaticData();
                            int userInput = -1;
                            int.TryParse(Console.ReadLine(), out userInput);
                            //userInput = 0;
                            if (userInput >= 0 && userInput <= 15)
                            {
                                //Console.WriteLine("Enter path where you want to save file :");
                                //filePath = Console.ReadLine();
                                filePath = System.Configuration.ConfigurationSettings.AppSettings.Get("FilePath").ToString();
                                if (!System.IO.Directory.Exists(filePath))
                                {
                                    System.IO.Directory.CreateDirectory(filePath);
                                }
                                //Console.WriteLine("Username:");
                                //string userName = Console.ReadLine();
                                //Console.WriteLine("Password:");
                                //string password = InputPassword();
                                string userName = System.Configuration.ConfigurationSettings.AppSettings.Get("UserName").ToString();
                                string password = System.Configuration.ConfigurationSettings.AppSettings.Get("Password").ToString();
                                Console.WriteLine("Data loading is in progress please wait.......");
                                JIRAManager manager = new JIRAManager(userName, password);
                                if (userInput == 0)
                                {
                                    printValue = CreateExcelForAllProjects(manager, filePath);
                                }
                                else
                                {
                                    printValue = FetchData(manager, userInput, filePath) == "OK" ? "Excel created successfully" : response;
                                }
                            }
                            else
                            {
                                printValue = "Wrong Input";
                            }
                        }
                        else
                        {
                            printValue = "Please check your internet connection";
                        }

                        Console.WriteLine(printValue);
                        Console.ReadLine();
                    }
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    Console.WriteLine("Microsoft Excel not installed");
                }
                catch (InvalidCastException)
                {
                    Console.WriteLine("Microsoft Excel not installed properly");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        private static string FetchData(JIRAManager manager, int userInput, string filePath, CreateExcelFile excel = null)
        {
            int startAt = 0;
            CreateExcelFile excelFile = null;
            string response = "OK";
            if (excel == null)
            {
                excelFile = new CreateExcelFile(GetJiraResource(userInput).ToString(), filePath);
                response = excelFile.CreatingExcelHeader();
            }
            else
            {
                excelFile = excel;
            }

            //excelFile = excel != null ? excel : new CreateExcelFile(GetJiraResource(userInput).ToString(), filePath);

            //response = excelFile.CreatingExcelHeader();
            if (response == "OK")
            {
                do
                {
                    dynamic dataFromJiraServer = manager.RunQuery(GetJiraResource(userInput), null, null, "GET", startAt);// + parameter startAt
                    if (dataFromJiraServer.ToString() == "Exception")
                    {
                        response = "It seems like your username or password is incorrect please verify it and please try again";
                        break;
                    }
                    //call fill excel
                    else
                    {
                        response = excelFile.PopulatingDataInExcel(dataFromJiraServer);
                        if (response == "OK")
                        {
                            if (Convert.ToInt32(dataFromJiraServer.startAt.ToString()) + 999 < Convert.ToInt32(dataFromJiraServer.total.ToString()))
                            {
                                startAt += 1000;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                while (true);
                if (excel == null)
                {
                    response = excelFile.SavingExcel();
                }
                //save excel
            }
            return response;
        }
        //call fetch data
        public static string CreateExcelForAllProjects(JIRAManager manager, string filePath)
        {
            string returnValue = String.Empty;
            CreateExcelFile excelFile = new CreateExcelFile(GetJiraResource(0).ToString(), filePath);
            returnValue = excelFile.CreatingExcelHeader();
            if (returnValue == "OK")
            {
                for (int count = 1; count <= 15; count++)
                {
                    Console.WriteLine("Fetching for " + count + " started");
                    //if (count == 6)
                    //{
                    returnValue = FetchData(manager, count, filePath, excelFile);
                    //}
                    Console.WriteLine("Fetching for " + count + " completed");
                }
            }
            if (returnValue == "OK")
            {
                excelFile.SavingExcel();
                returnValue = "Excel created successfully";
            }
            return returnValue;
        }
        public static void ShowStaticData()
        {
            Console.WriteLine("Press 0 for All Projects");
            Console.WriteLine("Press 1 for HRIS");
            Console.WriteLine("Press 2 for BCS");
            Console.WriteLine("Press 3 for VETS");
            Console.WriteLine("Press 4 for ORCA");
            Console.WriteLine("Press 5 for HomeStat");
            Console.WriteLine("Press 6 for ServiceNow");
            Console.WriteLine("Press 7 for CARES");
            Console.WriteLine("Press 8 for Shelter Capacity Dashboard");
            Console.WriteLine("Press 9 for Network, Security and Telecom Stabilization");
            Console.WriteLine("Press 10 for New Sharepoint Portal");
            Console.WriteLine("Press 11 for CAP-DASH");
            Console.WriteLine("Press 12 for HOME");
            Console.WriteLine("Press 13 for Inspection Mobile");
            Console.WriteLine("Press 14 for Inspections Web");
            Console.WriteLine("Press 15 for Quarterly Count / HOPE Web Application");
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("https://www.google.com/"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
        private static JiraResource GetJiraResource(int val)
        {
            JiraResource value = JiraResource.HRIS;
            switch (val)
            {
                case 1:
                    {
                        value = JiraResource.HRIS;
                        break;
                    }
                case 2:
                    {
                        value = JiraResource.BCSB;
                        break;
                    }
                case 3:
                    {
                        value = JiraResource.VTS;
                        break;
                    }
                case 4:
                    {
                        value = JiraResource.LO;
                        break;
                    }
                case 5:
                    {
                        value = JiraResource.HOMESTAT;
                        break;
                    }
                case 6:
                    {
                        value = JiraResource.SNIHD;
                        break;
                    }
                case 7:
                    {
                        value = JiraResource.CARES;
                        break;
                    }
                case 8:
                    {
                        value = JiraResource.SCDN;
                        break;
                    }
                case 9:
                    {
                        value = JiraResource.NTS;
                        break;
                    }
                case 10:
                    {
                        value = JiraResource.NSP;
                        break;
                    }
                case 11:
                    {
                        value = JiraResource.CD;
                        break;
                    }
                case 12:
                    {
                        value = JiraResource.HOME;
                        break;
                    }
                case 13:
                    {
                        value = JiraResource.IM;
                        break;
                    }
                case 14:
                    {
                        value = JiraResource.IW;
                        break;
                    }
                case 15:
                    {
                        value = JiraResource.QCHWA;
                        break;
                    }
                case 0:
                    {
                        value = JiraResource.ALL;
                        break;
                    }
            }
            return value;
        }
        private static string InputPassword()
        {
            StringBuilder password = new StringBuilder(string.Empty);
            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.LeftArrow || key.Key == ConsoleKey.RightArrow || key.Key == ConsoleKey.Tab || key.Key == ConsoleKey.Delete || key.Key == ConsoleKey.UpArrow
                    || key.Key == ConsoleKey.DownArrow || key.Key == ConsoleKey.Home || key.Key == ConsoleKey.End || key.Key == ConsoleKey.Insert || key.Key == ConsoleKey.PageUp
                    || key.Key == ConsoleKey.PageDown || key.Key == ConsoleKey.LeftWindows || key.Key == ConsoleKey.Escape || key.Key == ConsoleKey.F1 || key.Key == ConsoleKey.F2
                    || key.Key == ConsoleKey.F3 || key.Key == ConsoleKey.F4 || key.Key == ConsoleKey.F5 || key.Key == ConsoleKey.F6 || key.Key == ConsoleKey.F7 || key.Key == ConsoleKey.F8
                    || key.Key == ConsoleKey.F9 || key.Key == ConsoleKey.F10 || key.Key == ConsoleKey.F11 || key.Key == ConsoleKey.F12 || key.Key == ConsoleKey.Enter)
                {

                }
                else if (key.Key != ConsoleKey.Backspace)
                {
                    //password += key.KeyChar;
                    password.Append(key.KeyChar);
                    Console.Write("*");
                }
                else if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                {
                    password.Remove(password.Length - 1, 1);
                    Console.Write("\b");
                    Console.Write(" ");
                    Console.Write("\b");
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);
            Console.WriteLine();
            return password.ToString();
        }
    }
}
